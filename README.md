# Fritzy infra os docker upgrade

Upgrade first docker node found which has not the lastest docker-ce package.
Run this task as many nodes you have.
**Works on node with only swarm services** otherwise node will not be drain of external swarm docker containers and node will not be upgraded.

## common environment variables used to deploy

- DEPLOY_HOSTNAME_IDENTITY=**mandatory** *ansible ssh private key*
- HCLOUD_TOKEN=**mandatory** *hetzner api token*

### Deployment sequence

```plantuml
@startuml

[*] --> drain
drain : Drain swarm node

drain --> wait
wait: wait no services are running
wait: on the drained node
wait -l-> wait

wait --> upgrade
upgrade: Upgrade docker-ce apt package

upgrade --> active
active: Active swarm node

active --> [*]

@enduml
```
